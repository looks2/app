const request = require('supertest')
const app = require('./app')

describe('Test the root paht', () => {
    test('It should response the GET METHOD', async () => {
        const response = await request(app).get('/')
        expect(response.statusCode).toBe(200)
    })
})